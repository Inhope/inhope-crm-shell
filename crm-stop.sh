#!/bin/sh

~/software/tomcat-8080/bin/shutdown.sh
~/software/tomcat-8090/bin/shutdown.sh

ps -ef|grep tomcat-8080|grep -v grep|cut -c 7-15|xargs kill -9
ps -ef|grep tomcat-8089|grep -v grep|cut -c 7-15|xargs kill -9

# nginx -s stop
