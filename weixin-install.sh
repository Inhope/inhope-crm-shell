#!/bin/sh

# 下载仓库
cd ~/projects
git clone -b weixin-api git@bitbucket.org:Inhope/inhope-crm.git inhope-weixin

# 安装tomcat
cd ~/software
tar -xzvf tomcat-7.0.69.tar.gz
mv tomcat-7.0.69 tomcat-9000

rm -f ~/software/tomcat-9000/webapps/crm
rm -f ~/software/tomcat-9000/conf/server.xml
ln -s ~/shell/weixin-server-9000.xml ~/software/tomcat-9000/conf/server.xml
ln -s ~/projects/inhope-weixin ~/software/tomcat-9000/webapps/weixin

# nginx配置
ln -s ~/shell/weixin.conf /usr/local/nginx/conf/conf.d/weixin.conf