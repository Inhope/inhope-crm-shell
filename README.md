# 部署流程

1. 将软件scp到software下
2. 安装环境, 基础软件
3. 安装CRM
4. 安装CAS
5. 安装mule

## 1. 将软件scp到software下
复制所有软件到生产环境, 这些软件在测试环境的机器上, 软件包括nginx, tomcat, java, memcached, mule, git. 其中CRM, SSO机器不需要mule, 但为了方便, 可以全都复制过去. 这些软件在测试环境机器上

### 1.1 连接测试环境
```
ssh -p 27 root@121.201.14.216
cd ~/software
```
## 1.2 复制software_source文件夹到3台生产环境机器
```
scp -P 42 -r software_source root@121.201.14.216:/root
scp -P 40 -r software_source root@121.201.14.216:/root
scp -P 38 -r software_source root@121.201.14.216:/root
```

## 2. 安装环境, 基础软件
参考 `env-install.sh`, 在每个机器上运行该命令即可.

## 3. 安装CRM
参考 `crm-install.sh`, 在执行命令前请先仔细阅读, 其中可能要修改的部分在[]中

一些命令:

* memcached-start.sh 启动memcached
* crm-start.sh 启动CRM
* crm-stop.sh 停止CRM
* crm-restart.sh 重启CRM, (无宕机重启)
* crm-boot.sh 开机自启动脚本

第一次启动CRM, 请先运行`memcached-start.sh` 启动memcached, 再运行 `crm-start.sh`

如果服务更新, 如何更新并重启?

先到`~/projects/inhope-crm/`目录 `git pull`, 再执行`crm-restart.sh`

`/root/logs/crm` 这是CRM的法规访问日志, 小安需要查看, NAS目录

## 4. 安装CAS
参考 `cas-install.sh`, 在执行命令前请先仔细阅读, 其中可能要修改的部分在[]中

一些命令:

* memcached-start.sh 启动memcached
* cas-start.sh 启动CAS
* cas-stop.sh 停止CAS
* cas-restart.sh 重启CAS, (无宕机重启)
* cas-boot.sh 开机自启动脚本

第一次启动CAS, 请先运行`memcached-start.sh` 启动memcached, 再运行 `cas-start.sh`

如果服务更新, 如何更新并重启?

先到`~/projects/inhope-cas/`目录 `git pull`, 再执行`cas-restart.sh`

## 5. 安装Mule
参考 `mule-install.sh`, 在执行命令前请先仔细阅读, 其中可能要修改的部分在[]中

一些命令:

* mule-start.sh 启动mule
* mule-stop.sh 停止mule
* mule-restart.sh 重启mule, (无宕机重启)
* mule-boot.sh 开机自启动脚本

第一次启动mule, 运行 `mule-start.sh`

如果服务更新, 如何更新并重启?

mule项目有两个副本, 所以都要更新.

先执行`mule-pull.sh`, 再执行 `mule-restart.sh`

# 服务器目录结构

* ~/projects 项目目录, 软链接->/inhope_xiaoan/projects
* ~/software 软件安装目录,git,nginx,java,tomcat,mule
* ~/shell 脚本 (软链接)
* ~/logs 所有的日志目录, 有一些是软链接
* ~/conf.d nginx的配置文件目录 (软链接)
* nginx: /usr/local/nginx
* memcahced: /usr/local/memcached

# 其它的一些常用命令
* nginx # 启动nginx
* nginx -s reload # 重启nginx

# 仓库列表

* git@bitbucket.org:Inhope/inhope-crm.git
* git@bitbucket.org:Inhope/inhope-cas.git
* git@bitbucket.org:Inhope/inhope-mule.git
* git@bitbucket.org:Inhope/inhope-crm-shell.git 脚本及配置文件

