#!/bin/sh

# 下载仓库
mkdir -p /inhope_xiaoan/projects
ln -s /inhope_xiaoan/projects ~/projects
cd ~/projects
git clone git@bitbucket.org:Inhope/inhope-cas.git
git clone git@bitbucket.org:Inhope/inhope-crm-shell.git
ln -s ~/projects/inhope-crm-shell ~/shell

# 安装tomcat
cd ~/software
tar -xzvf tomcat-7.0.69.tar.gz
mv tomcat-7.0.69 tomcat-8080
cp -r tomcat-8080 tomcat-8090

## tomcat配置文件, 两个tomcat作负载均衡/不宕机启动

rm -f ~/software/tomcat-8080/conf/server.xml
rm -f ~/software/tomcat-8090/conf/server.xml
ln -s ~/shell/cas-server-8080.xml ~/software/tomcat-8080/conf/server.xml
ln -s ~/shell/cas-server-8090.xml ~/software/tomcat-8090/conf/server.xml

rm -f ~/software/tomcat-8080/conf/context.xml
rm -f ~/software/tomcat-8090/conf/context.xml
ln -s ~/shell/context-memcached.xml ~/software/tomcat-8080/conf/context.xml
ln -s ~/shell/context-memcached.xml ~/software/tomcat-8090/conf/context.xml

rm -f ~/software/tomcat-8080/webapps/crm
rm -f ~/software/tomcat-8090/webapps/crm

ln -s ~/projects/inhope-cas ~/software/tomcat-8080/webapps/cas
ln -s ~/projects/inhope-cas ~/software/tomcat-8090/webapps/cas

# nginx配置
cd /usr/local/nginx/conf
rm -f nginx.conf
ln -s ~/shell/nginx.conf ./nginx.conf
mkdir conf.d
ln -s ~/shell/mule.conf ./conf.d/mule.conf
ln -s /usr/local/nginx/conf/conf.d ~/conf.d

# cas配置 [deployerConfigContext.xml包含数据库配置, 请根据具体情况修改]

cd ~/projects/inhope-cas
cp configs/deployerConfigContext.xml ./WEB-INF/

# 日志目录配置

mkdir ~/logs

ln -s /usr/local/nginx/logs ~/logs/nginx
ln -s ~/software/tomcat-8080/logs ~/logs/tomcat-8080
ln -s ~/software/tomcat-8090/logs ~/logs/tomcat-8090

# 开机自启动
echo "sh /root/shell/cas-boot.sh" >> /etc/rc.local
chmod +x /etc/rc.local