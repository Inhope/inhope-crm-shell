#!/bin/sh

startTime=`date +"%s.%N"`  

~/software/tomcat-8080/bin/startup.sh
~/software/tomcat-8090/bin/startup.sh

echo "等待启动完成"
sleep 5;
curl -I http://localhost:8080/register

endTime=`date +"%s.%N"`   

echo `awk -v x1="$(echo $endTime | cut -d '.' -f 1)" -v x2="$(echo $startTime | cut -d '.' -f 1)" -v y1="$[$(echo $endTime | cut -d '.' -f 2) / 1000]" -v y2="$[$(echo $startTime | cut -d '.' -f 2) /1000]" 'BEGIN{printf "RunTime:%.6f s",(x1-x2)+(y1-y2)/1000000}'`  

nginx
