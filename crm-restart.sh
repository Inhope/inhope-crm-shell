#!/bin/sh

cd ~/projects/inhope-crm/WEB-INF/classes
rm -f log4j.properties
cp ../../configs/log4j.properties ./

echo "shutdown 8080"
~/software/tomcat-8080/bin/shutdown.sh
ps -ef|grep tomcat-8080|grep -v grep|cut -c 7-15|xargs kill -9
sleep 3;
echo "startup 8080"
~/software/tomcat-8080/bin/startup.sh

echo "等待启动完成"
sleep 5;
curl -I http://localhost:8080/register

echo "shutdown 8090"
~/software/tomcat-8090/bin/shutdown.sh
ps -ef|grep tomcat-8090|grep -v grep|cut -c 7-15|xargs kill -9
sleep 3;
echo "startup 8090"
~/software/tomcat-8090/bin/startup.sh

nginx -s reload

echo "成功"

ps -ef | grep tomcat