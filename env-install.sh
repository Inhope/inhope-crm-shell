#!/bin/sh

mv ~/software_source ~/software

## 2.1 安装公共依赖
yum -y install gcc
yum -y install gcc-c++ 
yum install make -y 

## 2.2 安装git依赖
yum install curl -y 
yum install curl-devel -y 
yum install zlib-devel -y 
yum install openssl-devel -y 
yum install perl -y 
yum install perl-ExtUtils-Embed -y
yum install cpio -y 
yum install expat-devel -y 
yum install gettext-devel -y 

## 2.3 安装git
cd ~/software
tar -xzvf git-1.7.6.tar.gz
cd git-1.7.6
autoconf
./configure
make
make install

# 2.4 nginx
cd ~/software
yum -y install pcre-devel
tar -xzvf nginx-1.8.1.tar.gz
cd nginx-1.8.1
./configure --with-http_stub_status_module --with-http_ssl_module 
make
sudo make install

# 2.5 java
cd ~/software
tar -xzvf jdk-7u79-linux-x64.tar.gz 

# 2.6 memcached
cd ~/software
tar -xzvf libevent-2.0.22-stable.tar.gz
cd libevent-2.0.22-stable
./configure --prefix=/usr/local/libevent
make
make install

cd ~/software
tar -xzvf memcached-1.4.25.tar.gz
cd memcached-1.4.25
sudo ./configure --prefix=/usr/local/memcached --with-libevent=/usr/local/libevent
make
make install

# 3. 设置环境变量
echo "export JAVA_HOME=/root/software/jdk1.7.0_79" >> ~/.bash_profile
echo "export PATH=\$PATH:\$JAVA_HOME/bin:/usr/local/nginx/sbin:/usr/local/memcached/bin:/root/shell" >> ~/.bash_profile

source ~/.bash_profile
