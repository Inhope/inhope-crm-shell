#!/bin/sh

echo "shutdown 1"
~/software/mule-1/bin/mule stop
echo "startup 1"
~/software/mule-1/bin/mule start

echo "为保证1成功运行, 等待60s"
secondsLeft=60
while [ $secondsLeft -gt 0 ];do  
  echo -n $secondsLeft  
  sleep 1 
  secondsLeft=$(($secondsLeft - 1))  
  echo -ne "\r     \r"
done 

echo "shutdown 2"
~/software/mule-2/bin/mule stop
echo "startup 2"
~/software/mule-2/bin/mule start

nginx -s reload

echo "成功"

ps -ef | grep mule