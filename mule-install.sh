#!/bin/sh

# 下载仓库
mkdir -p /inhope_xiaoan/projects
ln -s /inhope_xiaoan/projects ~/projects
cd ~/projects
git clone git@bitbucket.org:Inhope/inhope-mule.git inhope-mule-1
cp -r inhope-mule-1 inhope-mule-2
git clone git@bitbucket.org:Inhope/inhope-crm-shell.git
ln -s ~/projects/inhope-crm-shell ~/shell

# 安装mule环境
cd ~/software
rm -rf __MACOSX
unzip mule-standalone-3.7.0.zip
mv mule-standalone-3.7.0 mule-1
cp -r mule-1 mule-2

cd ~/software/mule-1/apps
rm -rf inhope-mule
ln -s ~/projects/inhope-mule-1 inhope-mule

cd ~/software/mule-2/apps
rm -rf inhope-mule
ln -s ~/projects/inhope-mule-2 inhope-mule

## inhope-mule配置
cd ~/projects/inhope-mule-1
rm -f classes/inhope.properties
cp classes/inhope-db.properties classes/inhope.properties # [inhope-db.properties 包含数据库配置, 请根据具体情况修改]
rm -f mule-app.properties
# [mule-app-1.properties 可能要修改的部分为: 1) webService表示小安法规库的服务地址, 比如128.1.1:8082 or c.a.com; 2) robotHost表示小安机器人的服务地址, 不要加端口号, 3) robotPort 为小安机器人的服务端口号, 比如8000; 
cp mule-app-1.properties mule-app.properties

cd ~/projects/inhope-mule-2
rm -f classes/inhope.properties
cp -u classes/inhope-db.properties classes/inhope.properties # [inhope-db.properties 包含数据库配置, 请根据具体情况修改]
rm -f mule-app.properties
# [mule-app-2.properties 可能要修改的部分为: 1) webService表示小安法规库的服务地址, 比如128.1.1:8082 or c.a.com; 2) robotHost表示小安机器人的服务地址, 不要加端口号, 3) robotPort 为小安机器人的服务端口号, 比如8000; 
cp -u mule-app-2.properties mule-app.properties

# nginx配置
cd /usr/local/nginx/conf
rm -f nginx.conf
ln -s ~/shell/nginx.conf ./nginx.conf
mkdir conf.d
ln -s ~/shell/mule.conf ./conf.d/mule.conf
ln -s /usr/local/nginx/conf/conf.d ~/conf.d

# 日志目录配置

mkdir ~/logs

ln -s /usr/local/nginx/logs ~/logs/nginx
ln -s ~/software/mule-1/logs ~/logs/mule-1
ln -s ~/software/mule-2/logs ~/logs/mule-2

# 开机自启动
echo "sh /root/shell/mule-boot.sh" >> /etc/rc.local
chmod +x /etc/rc.local
