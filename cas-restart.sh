#!/bin/sh

echo "shutdown 8080"
~/software/tomcat-8080/bin/shutdown.sh
echo "startup 8080"
~/software/tomcat-8080/bin/startup.sh

echo "等待启动完成"
sleep 5;
curl -I http://localhost:8080/register

echo "shutdown 8090"
~/software/tomcat-8090/bin/shutdown.sh
echo "startup 8090"
~/software/tomcat-8090/bin/startup.sh

nginx -s reload

echo "成功"

ps -ef | grep tomcat